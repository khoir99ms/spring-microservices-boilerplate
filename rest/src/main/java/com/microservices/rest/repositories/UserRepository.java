package com.microservices.rest.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import com.microservices.rest.models.Users;

import java.util.List;

@Repository
public class UserRepository {
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public List<Users> getUser(int id) {
		String sql = "SELECT id, name, email FROM users ";
		if (id > 0)
			sql += "WHERE id = " + id;
		
        List<Users> result = jdbcTemplate.query(
            sql,
            (rs, rowNum) -> new Users(
        		rs.getInt("id"),
        		rs.getString("name"), 
        		rs.getString("email")
            )
        );
        return result;
    }
	
	public int createUser(Users users) {
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		String sql = "INSERT INTO users (name, email) VALUES (:name, :email)";
        namedParameterJdbcTemplate.update(sql, new BeanPropertySqlParameterSource(users), keyHolder);
        
        return keyHolder.getKey().intValue();
    }
	
	public int modifyUser(Users users) {
		String sql = "UPDATE users set name = ?, email = ? WHERE id = ?";
        return jdbcTemplate.update(sql, users.getName(), users.getEmail(), users.getId());
    }
	
	public int removeUser(int id) {
		String sql = "DELETE FROM users WHERE id = ?";
        return jdbcTemplate.update(sql, id);
    }
}
