package com.microservices.rest.controllers;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import com.microservices.rest.models.Response;
import com.microservices.rest.models.Users;
import com.microservices.rest.repositories.UserRepository;


@RestController
@RequestMapping(path="/api/v1.0/user")
public class UserApi {
	final static Logger logger = Logger.getLogger(UserApi.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Response> get(
		@RequestParam(name = "id", required = false, defaultValue = "0") int id
	){
		try{
			return new ResponseEntity<>(new Response(userRepository.getUser(id), true, "ok"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Response> add(
		@RequestBody Users users
	){
		try{
			int lastId = userRepository.createUser(users);
			return new ResponseEntity<>(new Response(true, "user successfully inserted with id " + lastId), HttpStatus.CREATED);
		} catch (DuplicateKeyException e) {
			return new ResponseEntity<>(new Response("Duplicate entry for mail " + users.getEmail()), HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Response> update(
		@RequestBody Users users
	){
		try{
			if (userRepository.modifyUser(users) == 0) {
				throw new Exception("failed to update user.");
			}
			return new ResponseEntity<>(new Response(true, "user id "+ users.getId() +" successfully updated"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<Response> delete(
		@RequestParam(name = "id") int id
	){
		try{
			if (userRepository.removeUser(id) == 0) {
				throw new Exception("failed to delete user.");
			}
			return new ResponseEntity<>(new Response(true, "user id "+ id +" successfully deleted"), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(new Response(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
