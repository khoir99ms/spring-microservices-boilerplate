package com.microservices.rest.models;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Users {
	
	private Integer id;
	private String name;
	private String email;
	
	@JsonCreator
	public Users(
		@JsonProperty("id") int id, 
		@JsonProperty("name") String name, 
		@JsonProperty("email") String email
	){
        this.id = id;
        this.name = name;
        this.email = email;
    }
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Integer getId() {
		return this.id;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return this.email;
	}
}
