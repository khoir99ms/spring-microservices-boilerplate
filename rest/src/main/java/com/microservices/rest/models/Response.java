package com.microservices.rest.models;

import java.util.Collection;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Response {
	
	@JsonInclude(Include.NON_NULL)
	private int count;
	private Collection<?> data;
	@JsonInclude(Include.NON_NULL)
	private String message;
	private Boolean success = false;
	
	public Response() {}

	public Response(String message) {
		this.setMessage(message);
	}
	
	public Response(Collection<?> data, Boolean success) {
		this.setData(data);
		this.setSuccess(success);
	}
	
	public Response(Collection<?> data, Boolean success, String message) {
		this.setData(data);
		this.setSuccess(success);
		this.setMessage(message);
	}
	
	public Response(Boolean success, String message) {
		this.setSuccess(success);
		this.setMessage(message);
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Collection<?> getData() {
		return data;
	}

	public void setData(Collection<?> data) {
		this.data = data;
		this.setCount(data.size());
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}
	
}
